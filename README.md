# Selfie camera

-   Version 0.0.1

If you are looking for the English version, just scroll down

Данный проект является тестовым заданием и в нем проигнорированы некоторые огнаничения:

1. Приложение не взаимодействует с сервером
2. Не инкапсулирован css

Я буду благодарен за любую обратную связь.

This project is a test task, and some limitations were ignored.

1. The application does not interact with the server
2. Not encapsulated css

I would be grateful for any feedback.
