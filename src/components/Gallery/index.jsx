import React from 'react';
import PropTypes from 'prop-types';
import Photo from '../Photo';
import './index.css';

const Gallery = ({ photo, clearGallery }) => (
  <div className="gallery-wrapper">
    <div className="gallery-box">
      {
                photo[0]
                  ? photo.map(({ url, id }) => <Photo key={id} url={url} />)
                  : <div className="gallery-box-default">Фото появятся здесь</div>

                }
    </div>
    <button type="button" onClick={clearGallery} className="gallery-button">Очистить</button>
  </div>
);

Gallery.propTypes = {
  photo: PropTypes.arrayOf(PropTypes.object).isRequired,
  clearGallery: PropTypes.func.isRequired,
};


export default Gallery;
