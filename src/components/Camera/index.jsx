import React, { useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import './index.css';

const Camera = ({ streaming, selfieSwitch }) => {
  const ctx = {};
  const styles = streaming
    ? { display: 'block' }
    : { display: 'none' };
  const refVide = useCallback((node) => {
    if (node !== null && streaming) {
      node.srcObject = streaming;
      node.onloadedmetadata = () => {
        node.play();
      };
      ctx.video = node;
    }
  }, [streaming, ctx.video]);
  const canvas = useRef(null);
  const clickSelfieButton = () => selfieSwitch({ ...ctx, canvas });

  return (
    <div className="camera-wrapper">
      <div>
        <div className="camera-box">
          <video muted ref={refVide} style={styles} className="camera-video">Video stream not available.</video>
          <div style={styles} className="camera-mask" />

        </div>
        <button type="button" onClick={clickSelfieButton} className="camera-button">{streaming ? 'Фото' : 'Старт'}</button>
      </div>
      <canvas ref={canvas} className="camera-canvas"> </canvas>
    </div>
  );
};

Camera.propTypes = {
  streaming: PropTypes.objectOf().isRequired,
  selfieSwitch: PropTypes.func.isRequired,
};


export default Camera;
