import React, { useState } from 'react';
import CameraContainer from '../../containers/CameraContainer';
import GalleryContainer from '../../containers/GalleryContainer';
import './index.css';

const App = () => {
  const [state, setState] = useState({ streaming: false, photo: [] });
  const stateObj = { state, setState };
  return (
    <div className="App">
      <CameraContainer {...stateObj} />
      <GalleryContainer {...stateObj} />
    </div>
  );
};

export default App;
