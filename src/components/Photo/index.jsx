import React from 'react';
import PropTypes from 'prop-types';

const Photo = ({ url }) => (
  <img src={url} alt="auto selfie" />
);

Photo.propTypes = {
  url: PropTypes.string.isRequired,
};

export default Photo;
