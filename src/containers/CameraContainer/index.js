import Camera from '../../components/Camera';

export default ({ state, setState }) => {
  function getRandomID() {
    return [...Array(8)].map(() => Math.floor(Math.random() * 10)).join('');
  }
  function startStream() {
    try {
      return navigator.mediaDevices.getUserMedia({ video: true, audio: false });
    } catch (error) {
      console.log(error);
    }
    return false;
  }
  function stopStream() {
    state.streaming.getTracks().forEach((trak) => trak.stop());
  }
  function getPhotoFromVideo({ canvas: { current: canvas }, video }) {
    const context = canvas.getContext('2d');
    canvas.width = 373;
    canvas.height = 280;
    context.drawImage(video, 0, 0, 373, 280);
    return canvas.toDataURL('image/png');
  }

  async function selfieSwitch(ctx) {
    if (state.streaming) {
      setState({
        ...state,
        streaming: false,
        photo: [
          ...state.photo,
          {
            url: getPhotoFromVideo(ctx),
            id: getRandomID(),
          },
        ],
      });
      stopStream();
    } else {
      setState({
        ...state,
        streaming: await startStream(),
      });
    }
  }
  return Camera({ ...state, selfieSwitch });
};
