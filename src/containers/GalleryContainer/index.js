import Gallery from '../../components/Gallery';

export default ({ state, setState }) => {
  function clearGallery() {
    setState({ ...state, photo: [] });
  }

  return Gallery({ ...state, clearGallery });
};
